import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {ProgressView} from '@react-native-community/progress-view';

const ProgressViewScreen = () => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      if (progress < 1) {
        setProgress(prevProgress => {
          const newProgress = prevProgress + 0.1;
          return newProgress > 1 ? 1 : newProgress;
        });
      }
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, [progress]);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ProgressView
        style={{width: 200, marginBottom: 15}}
        progress={progress}
        progressTintColor="#007AFF"
        trackTintColor="#CCCCCC"
      />
      <Text>Progress: {Math.round(progress * 100)}%</Text>
      <TouchableOpacity onPress={() => setProgress(0)}>
        <Text style={styles.button}>Reset Progress</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 24,
  },
});

export default ProgressViewScreen;
