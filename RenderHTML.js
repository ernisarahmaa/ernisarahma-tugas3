import React from 'react';
import {View, StyleSheet} from 'react-native';
import HTML from 'react-native-render-html';

const RenderHTMLScreen = () => {
  const htmlContent = `<h1 style="text-align: center;">Hello, <em>React Native Render HTML</em></h1>
  <h2 style="text-align: center;">Welcome to React Native Render HTML</h2>`;

  return (
    <View style={styles.container}>
      <HTML source={{html: htmlContent}} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
});

export default RenderHTMLScreen;
