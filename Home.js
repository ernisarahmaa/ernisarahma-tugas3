import React from 'react';
import {ScrollView} from 'react-native';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Net Info')}>
          <Text style={styles.buttonText}>Net Info</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Date Time Picker')}>
          <Text style={styles.buttonText}>Date Time Picker</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Slider')}>
          <Text style={styles.buttonText}>Slider</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Geolocation')}>
          <Text style={styles.buttonText}>Geolocation</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Progress View')}>
          <Text style={styles.buttonText}>Progress View</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Progress Bar')}>
          <Text style={styles.buttonText}>Progress Bar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Clipboard')}>
          <Text style={styles.buttonText}>Clipboard</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Async Storage')}>
          <Text style={styles.buttonText}>Async Storage</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Snap Carousel')}>
          <Text style={styles.buttonText}>Snap Carousel</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Image Zoom Viewer')}>
          <Text style={styles.buttonText}>Image Zoom Viewer</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Linear Gradient')}>
          <Text style={styles.buttonText}>Linear Gradient</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Render HTML')}>
          <Text style={styles.buttonText}>Render HTML</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Share')}>
          <Text style={styles.buttonText}>Share</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Skeleton Placeholder')}>
          <Text style={styles.buttonText}>Skeleton Placeholder</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Webview')}>
          <Text style={styles.buttonText}>Webview</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Tooltip')}>
          <Text style={styles.buttonText}>Tooltip</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Vector Icons')}>
          <Text style={styles.buttonText}>Vector Icons</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F4F4F4',
  },
  scrollViewContainer: {
    flexDirection: 'row', // Atur tampilan menjadi baris
    flexWrap: 'wrap', // Mengizinkan wrap ke baris berikutnya jika tidak cukup ruang
    justifyContent: 'center', // Atur posisi tengah secara horizontal
  },
  button: {
    fontSize: 12,
    backgroundColor: '#2E3283',
    width: '60%',
    height: 43,
    borderRadius: 5,
    marginVertical: 10,
    padding: 10,
  },
  buttonText: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
  },
});

export default HomeScreen;
