import React, {useState} from 'react';
import {
  TextInput,
  Text,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const DateTimeScreen = () => {
  const [date, setDate] = useState(new Date());
  const [input, setInput] = useState('');
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [birthdateText, setBirthdateText] = useState('');
  const [showMessage, setShowMessage] = useState(false);

  const onChange = (event, selectedDate) => {
    if (event.type === 'set' && selectedDate) {
      const currentDate = selectedDate || date;
      setDate(currentDate);
      setShowDatePicker(false);
      setBirthdateText(
        currentDate.toLocaleDateString('en-US', {
          month: 'long',
          day: 'numeric',
          year: 'numeric',
        }),
      );
    } else {
      setShowDatePicker(false);
    }
  };

  const onChangeText = text => {
    setInput(text);
  };

  const showDatePickerHandler = () => {
    setShowDatePicker(true);
  };

  const showMessageHandler = () => {
    setShowMessage(true);
  };

  const resetMessageHandler = () => {
    setShowMessage(false);
    setInput('');
    setBirthdateText('');
  };

  return (
    <View style={styles.container}>
      {showMessage && (
        <View>
          <Text>
            Hello {input}, your birth date is {birthdateText}
          </Text>
        </View>
      )}
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          value={input}
          onChangeText={onChangeText}
          placeholder="Enter your name"
        />
      </View>
      <TouchableWithoutFeedback onPress={showDatePickerHandler}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            value={birthdateText}
            placeholder="Birth Date"
            editable={false}
          />
        </View>
      </TouchableWithoutFeedback>
      {showDatePicker && (
        <DateTimePicker
          value={date}
          mode="date"
          display="default"
          onChange={onChange}
          maximumDate={new Date()}
        />
      )}
      <TouchableOpacity onPress={showMessageHandler}>
        <Text style={styles.button}>Show Message</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={resetMessageHandler}>
        <Text style={styles.button}>Reset</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 43,
    width: 273,
    color: 'black',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 273,
    height: 43,
    marginTop: 17,
    padding: 18,
    fontSize: 12,
  },
  button: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 24,
  },
});

export default DateTimeScreen;
