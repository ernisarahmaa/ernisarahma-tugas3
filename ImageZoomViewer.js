import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ImageViewer} from 'react-native-image-zoom-viewer';

const images = [
  {
    url: 'https://i.pinimg.com/originals/08/36/24/083624fb6e86dd109938d74c845e8a23.jpg', // Ganti dengan URL gambar Anda
  },
  {
    url: 'https://i.pinimg.com/originals/05/80/ab/0580ab1dd94842e9558b80246f9bf53d.jpg', // Ganti dengan URL gambar Anda
  },
  {
    url: 'https://i.pinimg.com/originals/bd/12/8a/bd128af509671c2661756d32d9ddc780.jpg', // Ganti dengan URL gambar Anda
  },
];

function ImageViewerScreen() {
  return (
    <View style={styles.container}>
      <ImageViewer
        imageUrls={images}
        index={0} // Indeks gambar pertama yang akan ditampilkan
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default ImageViewerScreen;
