import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';

const {width: screenWidth} = Dimensions.get('window');

class SnapCarouselScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      entries: [
        {title: 'Item 1'},
        {title: 'Item 2'},
        {title: 'Item 3'},
        {title: 'Item 4'},
        {title: 'Item 5'},
      ],
      activeSlide: 0,
    };
  }

  _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Carousel
          ref={c => {
            this._carousel = c;
          }}
          data={this.state.entries}
          renderItem={this._renderItem}
          sliderWidth={screenWidth}
          itemWidth={200}
          onSnapToItem={index => this.setState({activeSlide: index})}
        />
        <Pagination
          dotsLength={this.state.entries.length}
          activeDotIndex={this.state.activeSlide}
          containerStyle={styles.pagination}
          dotStyle={styles.dot}
          inactiveDotStyle={styles.inactiveDot}
          inactiveDotOpacity={0.6}
          inactiveDotScale={0.8}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 50,
  },
  slide: {
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightgray',
    borderRadius: 10,
  },
  title: {
    fontSize: 24,
    color: 'black',
  },
  pagination: {
    position: 'absolute',
    bottom: 16,
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: 'blue',
  },
  inactiveDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: 'gray',
  },
});

export default SnapCarouselScreen;
