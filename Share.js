import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Share from 'react-native-share';

const ShareScreen = () => {
  const shareText = () => {
    const shareOptions = {
      title: 'Bagikan teks ini',
      message: 'Ini adalah teks yang ingin saya bagikan.',
    };

    Share.open(shareOptions)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.error(err);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.teks}>Bagikan teks ini</Text>
      <Text style={styles.teks}>Ini adalah teks yang ingin saya bagikan.</Text>
      <TouchableOpacity onPress={shareText}>
        <Text style={styles.button}>Bagikan Teks</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  teks: {
    marginVertical: 10,
    fontSize: 15,
  },
  button: {
    fontSize: 20,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 9,
    borderRadius: 5,
    marginTop: 15,
    fontWeight: 'bold',
  },
});

export default ShareScreen;
