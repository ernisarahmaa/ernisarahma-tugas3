import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Snackbar from 'react-native-snackbar'; // Import Snackbar

const showSnackbar = (message, duration = Snackbar.LENGTH_SHORT) => {
  Snackbar.show({
    text: message,
    duration: duration,
  });
};

const VectorIconScreen = () => {
  const handleIconPress = () => {
    console.log('Ikon diklik!');
    showSnackbar('Icon Clicked!');
  };

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity onPress={handleIconPress}>
        <Icon name="heart" size={50} color="#f8d1d5" />
      </TouchableOpacity>
      <Text>Klik ikon di atas</Text>
    </View>
  );
};

export default VectorIconScreen;
