import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import HomeScreen from './Home';
import NetInfoScreen from './NetInfo';
import DateTimeScreen from './DateTime';
import SliderScreen from './Slider';
import GeolocationScreen from './Geolocation';
import ProgressViewScreen from './ProgressView';
import ProgressBarScreen from './ProgressBar';
import ClipboardScreen from './Clipboard';
import AsyncStorageScreen from './AsyncStorage';
import SnapCarouselScreen from './SnapCarousel';
import ImageViewerScreen from './ImageZoomViewer';
import LinearGradientScreen from './LinearGradient';
import RenderHTMLScreen from './RenderHTML';
import ShareScreen from './Share';
import SkeletonScreen from './Skeleton';
import WebViewScreen from './WebView';
import TooltipScreen from './Tooltip';
import VectorIconsScreen from './VectorIcon';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Net Info" component={NetInfoScreen} />
        <Stack.Screen name="Date Time Picker" component={DateTimeScreen} />
        <Stack.Screen name="Slider" component={SliderScreen} />
        <Stack.Screen name="Geolocation" component={GeolocationScreen} />
        <Stack.Screen name="Progress View" component={ProgressViewScreen} />
        <Stack.Screen name="Progress Bar" component={ProgressBarScreen} />
        <Stack.Screen name="Clipboard" component={ClipboardScreen} />
        <Stack.Screen name="Async Storage" component={AsyncStorageScreen} />
        <Stack.Screen name="Snap Carousel" component={SnapCarouselScreen} />
        <Stack.Screen name="Image Zoom Viewer" component={ImageViewerScreen} />
        <Stack.Screen name="Linear Gradient" component={LinearGradientScreen} />
        <Stack.Screen name="Render HTML" component={RenderHTMLScreen} />
        <Stack.Screen name="Share" component={ShareScreen} />
        <Stack.Screen name="Skeleton Placeholder" component={SkeletonScreen} />
        <Stack.Screen name="Webview" component={WebViewScreen} />
        <Stack.Screen name="Tooltip" component={TooltipScreen} />
        <Stack.Screen name="Vector Icons" component={VectorIconsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
