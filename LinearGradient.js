import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const LinearGradientScreen = () => {
  return (
    <LinearGradient
      colors={[
        '#c9b1ff',
        '#ffcaf2',
        '#ffb2b1',
        '#fff3ad',
        '#bcffbc',
        '#a2edff',
      ]}
      style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.text}>Hello, Linear Gradient!</Text>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    backgroundColor: '#f8d1d5',
    padding: 20,
    borderRadius: 10,
  },
  text: {
    color: 'black',
    fontSize: 24,
  },
});

export default LinearGradientScreen;
