import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Tooltip from 'rn-tooltip';

const TooltipScreen = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Tooltip
        popover={<Text>This is a tooltip!</Text>}
        height={50}
        width={150}
        backgroundColor="#f8d1d5"
        skipAndroidStatusBar={false}
        overlayColor="transparent">
        <Text>Show Tooltip</Text>
      </Tooltip>
    </View>
  );
};

export default TooltipScreen;
