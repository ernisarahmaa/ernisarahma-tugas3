import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import Geolocation from '@react-native-community/geolocation';

function GeolocationScreen() {
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');
  const [status, setStatus] = useState('');

  const getLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        const currentLongt = JSON.stringify(position.coords.longitude);
        const currentLat = JSON.stringify(position.coords.latitude);
        setLatitude(currentLat);
        setLongitude(currentLongt);
      },
      error => {
        setStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        maximumAge: 1000,
      },
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Current Position</Text>
      <Text style={styles.textContent}>Latitude : {latitude}</Text>
      <Text style={styles.textContent}>Longitude : {longitude}</Text>
      <TouchableOpacity style={styles.GetPosition} onPress={getLocation}>
        <Text style={styles.button}>Get Position</Text>
      </TouchableOpacity>
      <Text>{status}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 20,
  },
  textContent: {
    fontSize: 15,
    color: 'black',
    marginBottom: 10,
  },
  button: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 20,
  },
});

export default GeolocationScreen;
