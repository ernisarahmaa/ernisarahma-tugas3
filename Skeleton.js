import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SkeletonScreen = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Simulasi pengambilan data asynchronous
    setTimeout(() => {
      setIsLoading(false);
    }, 3000); // Menggantinya dengan logika pengambilan data sebenarnya
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <SkeletonPlaceholder>
          <View style={styles.placeholderContainer}>
            <View style={styles.placeholderTitle} />
            <View style={styles.placeholderText} />
            <View style={styles.placeholderButton} />
          </View>
        </SkeletonPlaceholder>
      ) : (
        <View>
          <Text style={styles.title}>Data Loaded</Text>
          <Text style={styles.textContent}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </Text>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Click Me</Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  placeholderContainer: {
    width: 300,
    height: 200,
    borderRadius: 10,
    backgroundColor: '#E0E0E0',
    padding: 20,
  },
  placeholderTitle: {
    width: 150,
    height: 20,
    marginBottom: 10,
    backgroundColor: '#CCCCCC',
    borderRadius: 5,
  },
  placeholderText: {
    width: '100%',
    height: 50,
    marginBottom: 10,
    backgroundColor: '#CCCCCC',
    borderRadius: 5,
  },
  placeholderButton: {
    width: 100,
    height: 30,
    backgroundColor: '#CCCCCC',
    borderRadius: 5,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  textContent: {
    fontSize: 16,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#2E3283',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default SkeletonScreen;
