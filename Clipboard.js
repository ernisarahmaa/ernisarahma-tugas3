import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';

const ClipboardScreen = () => {
  const [copiedText, setCopiedText] = useState('');
  const [inputText, setInputText] = useState('');

  const copyToClipboard = () => {
    Clipboard.setString(inputText);
  };

  const fetchCopiedText = async () => {
    const text = await Clipboard.getString();
    setCopiedText(text);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <TextInput
          placeholder="Enter text to copy"
          value={inputText}
          onChangeText={setInputText}
          style={styles.input}
        />
        <TouchableOpacity onPress={copyToClipboard}>
          <Text style={styles.button}>Click here to copy to Clipboard</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={fetchCopiedText}>
          <Text style={styles.button}>View copied text</Text>
        </TouchableOpacity>
        <Text style={styles.copiedText}>{copiedText}</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 43,
    width: 273,
    backgroundColor: 'white',
    paddingLeft: 10,
  },
  button: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 20,
  },
  copiedText: {
    marginTop: 20,
    height: 43,
    width: 273,
    backgroundColor: 'white',
    textAlign: 'center',
    color: 'red',
    fontWeight: 'bold',
    paddingVertical: 10,
  },
});

export default ClipboardScreen;
