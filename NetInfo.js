import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import NetInfo from '@react-native-community/netinfo';

const NetInfoScreen = () => {
  const [type, setType] = React.useState(null);
  const [isConnected, setIsConnected] = React.useState(null);

  React.useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      setType(state.type);
      setIsConnected(state.isConnected);
    });
    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Network Type: {type}</Text>
      <Text style={styles.text}>
        Network Status: {isConnected ? 'Connected' : 'Disconnected'}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    margin: 10,
  },
});

export default NetInfoScreen;
