import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar'; // Import Snackbar

const AsyncStorageScreen = () => {
  const [inputText, setInputText] = useState('');
  const [storedText, setStoredText] = useState('');

  useEffect(() => {
    // Mengambil teks dari AsyncStorage saat komponen pertama kali dimuat
    retrieveText();
  }, []);

  const showSnackbar = (message, duration = Snackbar.LENGTH_SHORT) => {
    Snackbar.show({
      text: message,
      duration: duration,
    });
  };

  const storeText = async () => {
    try {
      // Menyimpan teks ke AsyncStorage
      await AsyncStorage.setItem('storedText', inputText);
      console.log('Teks berhasil disimpan!');
      setStoredText(inputText);
      showSnackbar('Teks berhasil disimpan!'); // Menampilkan Snackbar sukses
    } catch (error) {
      console.error('Gagal menyimpan teks:', error);
      showSnackbar('Gagal menyimpan teks.', Snackbar.LENGTH_LONG); // Menampilkan Snackbar gagal
    }
  };

  const retrieveText = async () => {
    try {
      // Mengambil teks dari AsyncStorage
      const text = await AsyncStorage.getItem('storedText');
      if (text !== null) {
        setStoredText(text);
      }
    } catch (error) {
      console.error('Gagal mengambil teks:', error);
    }
  };

  return (
    <View style={styles.container}>
      <Text>Masukkan teks:</Text>
      <TextInput
        placeholder="Ketik disini"
        onChangeText={text => setInputText(text)}
        value={inputText}
        style={styles.input}
      />
      <TouchableOpacity onPress={storeText}>
        <Text style={styles.button}>Simpan Teks</Text>
      </TouchableOpacity>
      <Text>Teks yang disimpan:</Text>
      <Text style={styles.storedText}>{storedText}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 43,
    width: 273,
    backgroundColor: 'white',
    textAlign: 'center',
    marginTop: 20,
  },
  button: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginVertical: 20,
  },
  storedText: {
    marginTop: 20,
    height: 43,
    width: 273,
    backgroundColor: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    paddingVertical: 10,
  },
});

export default AsyncStorageScreen;
